import { useEffect, useState } from "react";
import ErrorNotification from "./ErrorNotification";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Homepage from "./HomePage/Homepage.js";
import Login from "./AuthPage/Login.js";
import SignUp from "./AuthPage/SignUp.js";

import MyWatchList from "./WatchlistPage/MyWatchlist.js";
import MovieDetailPage from "./MovieDetailPage/MovieDetailPage";
import Account from "./UserPage/UserPage.js";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="login" element={<Login />} />
        <Route path="/" element={<Homepage />} />
        <Route path="signup" element={<SignUp />} />
        <Route path="mywatchlist" element={<MyWatchList />} />
        <Route
          path="/moviedetail/:movie_api_id"
          element={<MovieDetailPage />}
        />
        <Route path="account" element={<Account />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
<></>;
