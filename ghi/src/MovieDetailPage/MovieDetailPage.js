import CommentSection from "./CommentSection";
import { useParams } from "react-router-dom";
import Nav from "../HomePage/Nav";
import Banner from "./Banner";
import Trailer from "./Trailer";
import Footer from "../HomePage/Footer_Footer";
import MoviePictures from "./MoviePictures";

function MovieDetailPage() {
  const { movie_api_id } = useParams();
  console.log();

  return (
    <>
      <Nav />
      <Banner key="1" movie_api_id={movie_api_id} />

      <Trailer movie_api_id={movie_api_id} />

      <MoviePictures movie_api_id={movie_api_id} />
      <CommentSection movie_api_id={movie_api_id} />
      <Footer />
    </>
  );
}
export default MovieDetailPage;
