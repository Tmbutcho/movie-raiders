import { useGetBackdropsByApiIdQuery } from "../store/ApiMovies";
import "./MoviePictures.css";
import requests from "../request";
function MoviePictures(props) {
  const { movie_api_id } = props;
  const { data: content } = useGetBackdropsByApiIdQuery(movie_api_id);
  return (
    <>
      <h1 className="mv-title__component">Pictures</h1>
      <div className="mv-container__moviePictures">
        {content?.map((movie) => {
          return (
            <img
              key={movie.file_path}
              className="mv-picture"
              src={`${requests.baseImageUrl}${movie.file_path}`}
              alt=""
            ></img>
          );
        })}
      </div>
    </>
  );
}
export default MoviePictures;
