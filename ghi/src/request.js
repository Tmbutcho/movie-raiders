const requests = {
  fetchNowPlaying: "/TmdbApi/list_now_playing_movie",
  baseImageUrl: "https://image.tmdb.org/t/p/original",
};

export default requests;
