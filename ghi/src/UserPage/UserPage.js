import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import {
  useUpdateEmailMutation,
  useUpdateUsernameMutation,
  useGetCommentsByAccountIdQuery,
  useGetAllGenresQuery,
  useFavoriteGenresByIdMutation,
  useCreateFavoriteGenresMutation,
  useGetFavMovieQuery,
  useDeleteFavoriteGenresMutation,
} from "../store/ApiMovies";
import "./userPage.css";
import Nav from "../Nav/Nav";

function Account() {
  const [newEmail, setNewEmail] = useState("");
  const [newUsername, setNewUsername] = useState("");
  const [userComments, setUserComments] = useState([]);
  const [emailSuccess, setEmailSuccess] = useState(false);
  const [usernameSuccess, setUsernameSuccess] = useState(false);

  const [updateEmail] = useUpdateEmailMutation();
  const [updateUsername] = useUpdateUsernameMutation();
  const { data: commentsData, error: commentsError } =
    useGetCommentsByAccountIdQuery();
  const { data: genresData, error: genresError } = useGetAllGenresQuery();
  const { data: favGenresData, error: favoriteGenresError } =
    useGetFavMovieQuery();

  const [favoriteGenresData, setFavoriteGenresData] = useState([]);
  const [selectedGenres, setSelectedGenres] = useState([]);
  const [clickedButton, setClickedButton] = useState(null);

  const [favoriteGenresById] = useFavoriteGenresByIdMutation();
  const [createFavoriteGenres] = useCreateFavoriteGenresMutation();
  const [deleteFavoriteGenres] = useDeleteFavoriteGenresMutation();

  const [errorVisible, setErrorVisible] = useState(false);

  const navigate = useNavigate();

  const toggleFavoriteGenre = (genre) => {
    const isGenreSelected = selectedGenres.some(
      (selectedGenre) => selectedGenre.id === genre.id
    );

    const isGenreInFavorites = favoriteGenresData.some(
      (favGenre) => favGenre.api_genre_id === genre.api_genre_id
    );

    if (isGenreInFavorites) {
      setErrorVisible(true);
      return;
    }

    if (!isGenreSelected) {
      setSelectedGenres([...selectedGenres, genre]);
    }
  };

  const handleSubmitFavoriteGenres = async () => {
    try {
      const newFavoriteGenres = [];
      for (const genre of selectedGenres) {
        const isGenreInFavorites = favoriteGenresData.some(
          (favGenre) => favGenre.api_genre_id === genre.api_genre_id
        );

        if (!isGenreInFavorites) {
          const payload = {
            api_genre_id: genre.api_genre_id,
          };

          const response = await createFavoriteGenres(payload);

          if (response.data) {
            console.log(
              `Favorite genre ${genre.api_genre_id} submitted successfully!`
            );
            newFavoriteGenres.push({
              id: response.data.id,
              name: genre.name,
            });
          }
        }
      }
      setFavoriteGenresData([...favoriteGenresData, ...newFavoriteGenres]);

      setSelectedGenres([]);
      setErrorVisible(false);
    } catch (err) {
      console.error("Failed to submit favorite genres:", err);
    }
  };

  const handleDeleteFavoriteGenre = async (genreId) => {
    try {
      await deleteFavoriteGenres(genreId);

      setFavoriteGenresData((prevGenres) =>
        prevGenres.filter((genre) => genre.id !== genreId)
      );
    } catch (err) {
      console.error("Failed to delete favorite genre:", err);
    }
  };

  useEffect(() => {
    if (favGenresData && Array.isArray(favGenresData.favorite_genres)) {
      setFavoriteGenresData(favGenresData.favorite_genres);
    }
  }, [favGenresData]);

  useEffect(() => {
    if (commentsData) {
      setUserComments(commentsData);
    }
  }, [commentsData]);

  useEffect(() => {
    if (genresData) {
      setSelectedGenres([]);
    }
  }, [genresData]);

  const handleEmailUpdate = async (e) => {
    e.preventDefault();
    try {
      await updateEmail({ email: newEmail });
      setEmailSuccess(true);
      setNewEmail("");
    } catch (err) {
      console.error("Failed to update email:", err);
    }
  };

  const handleUsernameUpdate = async (e) => {
    e.preventDefault();
    try {
      await updateUsername({ username: newUsername });
      setUsernameSuccess(true);
      setNewUsername("");
    } catch (err) {
      console.error("Failed to update username:", err);
    }
  };

  return (
    <>
      <Nav />
      <div className="user-page-container">
        <div className="row justify-content-center">
          <div className="col-md-8">
            <div className="user-card shadow p-4 mt-4">
              <h1 className="centered-text">Account</h1>
              <h2>Email Update</h2>
              {emailSuccess && (
                <div className="alert alert-success">
                  Email updated successfully
                </div>
              )}
              <form onSubmit={handleEmailUpdate}>
                <div className="form-floating mb-3">
                  <input
                    type="email"
                    value={newEmail}
                    onChange={(e) => setNewEmail(e.target.value)}
                    className="form-control"
                    id="newEmail"
                    required
                  />
                  <label htmlFor="newEmail">New Email</label>
                  <button className="btn btn-primary update-button">
                    Update Email
                  </button>
                </div>
              </form>
              <h2 className="centered-text move-left">Username Update</h2>
              {usernameSuccess && (
                <div className="alert alert-success">
                  Username updated successfully
                </div>
              )}
              <form onSubmit={handleUsernameUpdate}>
                <div className="form-floating mb-3">
                  <input
                    type="text"
                    value={newUsername}
                    onChange={(e) => setNewUsername(e.target.value)}
                    className="form-control"
                    id="newUsername"
                    required
                  />
                  <label htmlFor="newUsername">New Username</label>
                  <button className="btn btn-primary update-button">
                    Update Username
                  </button>
                </div>
              </form>
              <h2 className="centered-text">Choose Your Favorite Genres</h2>
              <div>
                {genresError ? (
                  <div>Error loading genres: {genresError.message}</div>
                ) : (
                  genresData && (
                    <div className="custom-button-container">
                      <div className="row first-row-buttons">
                        {genresData.api_genres.slice(0, 5).map((genre) => (
                          <div className="col-md-2 mb-3" key={genre.id}>
                            <button
                              className={`btn btn-primary custom-button ${
                                selectedGenres.some(
                                  (selectedGenre) =>
                                    selectedGenre.id === genre.id
                                )
                                  ? "active"
                                  : ""
                              }`}
                              onClick={() => toggleFavoriteGenre(genre)}
                            >
                              <span
                                className={`custom-button-text ${
                                  clickedButton === genre.id ? "text-black" : ""
                                }`}
                              >
                                {genre.name}
                              </span>
                            </button>
                          </div>
                        ))}
                      </div>
                      <div className="row">
                        {genresData.api_genres.slice(5, 11).map((genre) => (
                          <div className="col-md-2 mb-3" key={genre.id}>
                            <button
                              className={`btn btn-primary custom-button ${
                                selectedGenres.some(
                                  (selectedGenre) =>
                                    selectedGenre.id === genre.id
                                )
                                  ? "active"
                                  : ""
                              }`}
                              onClick={() => toggleFavoriteGenre(genre)}
                            >
                              <span
                                className={`custom-button-text ${
                                  clickedButton === genre.id ? "text-black" : ""
                                }`}
                              >
                                {genre.name}
                              </span>
                            </button>
                          </div>
                        ))}
                      </div>
                      <div className="row third-row-buttons">
                        {genresData.api_genres.slice(11, 16).map((genre) => (
                          <div className="col-md-2 mb-3" key={genre.id}>
                            <button
                              className={`btn btn-primary custom-button ${
                                selectedGenres.some(
                                  (selectedGenre) =>
                                    selectedGenre.id === genre.id
                                )
                                  ? "active"
                                  : ""
                              }`}
                              onClick={() => toggleFavoriteGenre(genre)}
                            >
                              <span
                                className={`custom-button-text ${
                                  clickedButton === genre.id ? "text-black" : ""
                                }`}
                              >
                                {genre.name}
                              </span>
                            </button>
                          </div>
                        ))}
                      </div>
                    </div>
                  )
                )}
              </div>
              {errorVisible && (
                <div className="alert alert-danger">
                  You already liked this genre
                </div>
              )}
              <div className="text-center mt-4">
                <button
                  className="btn btn-primary submit-button"
                  onClick={() => {
                    handleSubmitFavoriteGenres();
                    navigate("/account");
                  }}
                >
                  Submit Favorite Genres
                </button>
              </div>
              <div className="space-between-list-container">
                <h2>Your Favorite Genres</h2>
                <ul>
                  {favoriteGenresData && favoriteGenresData.length > 0 ? (
                    <div>
                      <ul>
                        {favoriteGenresData.map((genre) => (
                          <li key={genre.id}>
                            {genre.name}
                            <button
                              className="btn btn-danger btn-sm ml-2 delete-button"
                              onClick={() =>
                                handleDeleteFavoriteGenre(genre.id)
                              }
                            >
                              Delete
                            </button>
                          </li>
                        ))}
                      </ul>
                    </div>
                  ) : (
                    <p>No favorite genres found.</p>
                  )}
                </ul>
              </div>
              <h2 className="comments-heading move-left">Comment History:</h2>
              {commentsError ? (
                <div>No comments {commentsError.message}</div>
              ) : (
                <ul>
                  {userComments.map((comment) => (
                    <li key={comment.id}>
                      {comment.comment}{" "}
                      <button
                        className="btn btn-link custom-link"
                        onClick={() =>
                          navigate(`/moviedetail/${comment.movie_api_id}`)
                        }
                      >
                        Go To Movie
                      </button>
                    </li>
                  ))}
                </ul>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Account;
