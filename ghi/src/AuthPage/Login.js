import { useLoginMutation } from "../store/ApiMovies";
import { useState } from "react";
import Nav from "../Nav/Nav";
import { useNavigate } from "react-router-dom";
import "./Login.css";
import "../assets/footer-bg.jpg";

function Login() {
  const [login, loginResponse] = useLoginMutation();

  const [username, setLogin] = useState("");
  const userNameHandler = (event) => {
    const value = event.target.value;
    setLogin(value);
  };
  const [password, setPassword] = useState("");
  const passwordHandler = (event) => {
    const value = event.target.value;
    setPassword(value);
  };

  const [errorMessage, setErrorMessage] = useState("");
  const navigate = useNavigate();

  async function submitHandler(event) {
    event.preventDefault();

    const response = await login({ username, password });

    if (response.isError || !response.data) {
      setErrorMessage("You have entered the wrong username/password");
    } else {
      setErrorMessage("");
      setLogin("");
      setPassword("");
      navigate("/");
    }
  }

  return (
    <>
      <Nav />
      <div className="login-container">
        <div className="row justify-content-center align-items-center">
          <div className="movie-login-card shadow p-4">
            <h1 id="signin__title">Login</h1>
            {errorMessage && (
              <div className="login-alert alert alert-danger">
                {errorMessage}
              </div>
            )}
            <form onSubmit={submitHandler} id="movie-create-location-form">
              <div className="movie-login-form-floating mb-3">
                <input
                  placeholder="Name"
                  required
                  type="text"
                  name="username"
                  id="username"
                  className="form-control"
                  onChange={userNameHandler}
                  value={username}
                />
                <label htmlFor="name">Username</label>
              </div>
              <div className="movie-login-form-floating mb-3">
                <input
                  placeholder="Password"
                  required
                  type="password"
                  name="password"
                  id="Password"
                  className="form-control"
                  onChange={passwordHandler}
                  value={password}
                />
                <label htmlFor="roomCount">Password</label>
              </div>

              <div className="mb-3"></div>
              <button className="movie-login-btn-dark-blue btn" type="submit">
                Login
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;
