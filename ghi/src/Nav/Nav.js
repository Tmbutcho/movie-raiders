import { NavLink } from "react-router-dom";
import { useGetTokenQuery } from "../store/ApiMovies";
import { useLogoutMutation } from "../store/ApiMovies";
import "./Nav.css";

function Nav() {
  const { data: account } = useGetTokenQuery();
  const [logout] = useLogoutMutation();

  return (
    <>
      <header className="header__navigation">
        <div className="header__navigation_NavLink-div">
          <NavLink to="/" className="header__navigation-NavLink">
            MOVIE RAIDERS
          </NavLink>
        </div>

        <nav className="nav">
          <div className="nav__links">
            <ul>
              {!account ? (
                <>
                  <li className="header__navigation-NavLink">
                    <NavLink
                      to="/signup"
                      className="header__navigation-NavLink green"
                      id="positive"
                    >
                      Sign Up
                    </NavLink>
                  </li>
                </>
              ) : (
                <>
                  <li>
                    <NavLink
                      to="/mywatchlist"
                      className="header__navigation-NavLink"
                    >
                      Watch List
                    </NavLink>
                  </li>
                  <li className="header__navigation-NavLink">
                    <NavLink
                      to="/account"
                      className="header__navigation-NavLink"
                    >
                      Account
                    </NavLink>
                  </li>
                </>
              )}
            </ul>
          </div>
        </nav>
        <div className="authentications">
          {account ? (
            <>
              <NavLink
                className="header__navigation-NavLink"
                id="negative"
                onClick={() => logout()}
              >
                Logout
              </NavLink>
            </>
          ) : (
            <>
              <NavLink
                className="header__navigation-NavLink"
                id="positive"
                to="/login"
              >
                Login
              </NavLink>
            </>
          )}
        </div>
      </header>
    </>
  );
}

export default Nav;
