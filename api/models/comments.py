from pydantic import BaseModel


class Message(BaseModel):
    message: str


class CommentIn(BaseModel):
    comment: str
    movie_api_id: str


class CommentUpdate(BaseModel):
    updated_comment: str


class CommentOutBase(BaseModel):
    id: str
    comment: str
    movie_api_id: str


class CommentOut(CommentOutBase):
    account_id: int


class CommentOutWithUsername(CommentOutBase):
    username: str
