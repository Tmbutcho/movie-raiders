from fastapi import APIRouter, Depends
from typing import List
from authenticator import authenticator
from models.comments import (
    CommentIn,
    CommentOut,
    CommentUpdate,
    Message,
    CommentOutWithUsername,
)
from queries.comments import CommentRepository

router = APIRouter()


@router.post("/api/comments", response_model=CommentOut)
def create_comment(
    comment: CommentIn,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.create_comment(comment, account_id)


@router.get("/api/comments/mine", response_model=List[CommentOut])
def get_comments_by_account_id(
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.get_comments_by_account_id(account_id)


@router.patch("/api/comments/{comment_id}", response_model=CommentOut)
def update_comment(
    comment_id: int,
    updated_comment_data: CommentUpdate,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.update_comment(comment_id, updated_comment_data, account_id)


@router.delete("/api/comments/{comment_id}", response_model=Message)
def delete_comment(
    comment_id: int,
    repo: CommentRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.delete_comment(comment_id, account_id)


@router.get(
    "/api/movies/{movie_api_id}/comments",
    response_model=List[CommentOutWithUsername],
)
def get_comments_by_movie_api_id(
    movie_api_id: int,
    repo: CommentRepository = Depends(),
):
    return repo.get_comments_by_movie_api_id(movie_api_id)
