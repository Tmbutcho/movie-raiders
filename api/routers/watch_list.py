from fastapi import APIRouter, Depends, Response
from authenticator import authenticator
from models.watch_list import (
    Message,
    WatchListIn,
    WatchListOut,
    WatchListArray,
)
from queries.watch_list import (
    WatchListRepository,
)

router = APIRouter()


@router.post("/api/watchlists", response_model=WatchListOut)
def create_watch_list(
    watch_list: WatchListIn,
    repo: WatchListRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.create_watch_list(watch_list, account_id)


@router.get("/api/watchlists/mine", response_model=WatchListArray)
def get_watch_list_by_account_id(
    repo: WatchListRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.get_watch_list_by_account_id(account_id)


@router.delete(
    "/api/watchlists/{watch_list_item_id:int}",
    response_model=Message,
)
def delete_watch_list_item(
    watch_list_item_id: int,
    repo: WatchListRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.delete_watch_list_item(watch_list_item_id, account_id)


@router.get(
    "/api/watchlists/mine/onlyApiid",
)
def get_watch_list_by_account_id_only_movie_id(
    repo: WatchListRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.get_watch_list_by_account_id_only_movie_id(account_id)
