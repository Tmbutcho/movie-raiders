from fastapi import APIRouter, Depends
from models.tmdbAPI import GenresListOfMovie
from queries.tmdbAPI import TMDBRepository


router = APIRouter()


@router.get("/api/TmdbApi/sort_by_Api_id/{api_id}")
def list_movie_sorted_by_apiId(api_id: int, repo: TMDBRepository = Depends()):
    return repo.list_movie_sorted_by_apiId(api_id)


@router.get("/api/TmdbApi/sort_by_genresId/{genre_id}")
def list_movie_sorted_by_apiId(
    genre_id: int, repo: TMDBRepository = Depends()
):
    return repo.list_movie_sorted_by_genresId(genre_id)


@router.get("/api/TmdbApi/list_now_playing_movie")
def list_now_playing_movie(repo: TMDBRepository = Depends()):
    return repo.list_now_playing_movie()


@router.get("/api/TmdbApi/top_rated")
def list_top_rated_movie(repo: TMDBRepository = Depends()):
    return repo.list_top_rated_movie()


@router.get("/api/TmdbApi/popular")
def list_popular_movie(repo: TMDBRepository = Depends()):
    return repo.list_popular_movie()


@router.get("/api/TmdbApi/upcoming")
def list_upcoming_movie(repo: TMDBRepository = Depends()):
    return repo.list_upcoming_movie()


@router.get("/api/TmdbApi/credits/{api_id}")
def cast_by_movie_id(api_id: int, repo: TMDBRepository = Depends()):
    return repo.cast_by_movie_id(api_id)


@router.get("/api/TmdbApi/videos/{api_id}")
def get_video_by_movie_id(api_id: int, repo: TMDBRepository = Depends()):
    return repo.get_video_by_movie_id(api_id)


@router.get("/api/TmdbApi/backdrops/{api_id}")
def get_backdrops_by_movie_id(api_id: int, repo: TMDBRepository = Depends()):
    return repo.get_backdrops_by_movie_id(api_id)


@router.get("/api/TmdbApi/keywords/{keywords}")
def get_movie_by_keywords(keywords: str, repo: TMDBRepository = Depends()):
    return repo.get_movie_by_keywords(keywords)
