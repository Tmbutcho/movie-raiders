import os
from psycopg_pool import ConnectionPool

pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))
from models.accounts import (
    AccountIn,
    AccountOut,
    AccountOutWithHashedPassword,
    Message,
)
from typing import Union


class AccountQueries:
    @property
    def collection(self):
        return pool["accounts"]

    def create_account(
        self, account: AccountIn, hashed_password: str
    ) -> Union[AccountOutWithHashedPassword, Message]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """

                    INSERT INTO accounts
                        (username, hashed_password, email)
                    VALUES
                        (%s, %s, %s)
                    RETURNING id;
                    """,
                    [account.username, hashed_password, account.email],
                )
                id = result.fetchone()[0]
                return self.account_in_to_out(id, account, hashed_password)

    def get_account(self, username: str):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id, username, hashed_password, email FROM accounts
                    WHERE username=%s
                    """,
                    [username],
                )
                content = result.fetchone()
                content_dic = {
                    "id": 0,
                    "username": "NONE",
                    "hashed_password": "NONE",
                    "email": "NONE",
                }
                i = 0
                for key in content_dic.keys():
                    content_dic[key] = content[i]
                    i += 1
        return AccountOutWithHashedPassword(**content_dic)

    def account_in_to_out(
        self, id: int, account: AccountIn, hashed_password: str
    ):
        old_data = account.dict()
        return AccountOutWithHashedPassword(
            id=id, hashed_password=hashed_password, **old_data
        )

    def update_password(
        self, account_id: int, hashed_password: str
    ) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    UPDATE accounts
                    SET hashed_password = %s
                    WHERE id = %s
                    RETURNING id, username, email;
                    """,
                    [hashed_password, account_id],
                )
                data = result.fetchone()

                return AccountOutWithHashedPassword(
                    id=data[0],
                    username=data[1],
                    email=data[2],
                    hashed_password=hashed_password,
                )

    def update_email(self, account_id: int, email: str) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    UPDATE accounts
                    SET email = %s
                    WHERE id = %s
                    RETURNING id, username, hashed_password, email;
                    """,
                    [email, account_id],
                )
                data = result.fetchone()

                return AccountOut(
                    id=data[0],
                    username=data[1],
                    hashed_password=data[2],
                    email=email,
                )

    def update_username(self, account_id: int, username: str) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    UPDATE accounts
                    SET username = %s
                    WHERE id = %s
                    RETURNING id, email, hashed_password, username;
                    """,
                    [username, account_id],
                )
                data = result.fetchone()

                return AccountOut(
                    id=data[0],
                    email=data[1],
                    hashed_password=data[2],
                    username=username,
                )
