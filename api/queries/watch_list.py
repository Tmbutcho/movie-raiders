from queries.pool import pool
from models.watch_list import (
    Message,
    WatchListOut,
    WatchListIn,
    WatchListArray,
)


class WatchListRepository:
    def get_watch_list_by_account_id(self, account_id: int):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT * FROM watch_lists
                    WHERE account_id = %s;
                    """,
                    (account_id,),
                )
                watch_lists = []
                for watch_list in result:
                    watch_list_obj = WatchListOut(
                        movie_api_id=watch_list[1],
                        account_id=watch_list[2],
                        id=watch_list[0],
                    )
                    watch_lists.append(watch_list_obj)

                watch_list_array = WatchListArray(watch_lists=watch_lists)
                return watch_list_array

    def create_watch_list(
        self, watch_list: WatchListIn, account_id: int
    ) -> WatchListOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO watch_lists(movie_api_id, account_id)
                    VALUES(%s, %s)
                    RETURNING id;
                    """,
                    (watch_list.movie_api_id, account_id),
                )
                id = result.fetchone()[0]
                watch_list_out = WatchListOut(
                    id=id,
                    movie_api_id=watch_list.movie_api_id,
                    account_id=account_id,
                )
                return watch_list_out

    def delete_watch_list_item(
        self, watch_list_item_id: int, account_id: int
    ) -> Message:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE  FROM watch_lists
                    WHERE id = %s AND account_id =%s;
                    """,
                    (watch_list_item_id, account_id),
                )
        return Message(message="Watch list item was sucessfully deleted")

    def get_watch_list_by_account_id_only_movie_id(self, account_id: int):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT movie_api_id
                    FROM watch_lists
                    WHERE account_id = %s;
                    """,
                    (account_id,),
                )
                list = []
                for i in result:
                    list.append(i[0])
                print(list)
                return list
