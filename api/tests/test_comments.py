from fastapi.testclient import TestClient
from queries.comments import CommentRepository
from dotenv import load_dotenv
import os


dotenv_path = os.path.join(os.path.dirname(__file__), "../../", ".env")
load_dotenv(dotenv_path)
SIGNING_KEY = os.getenv("SIGNING_KEY")
DATABASE_URL = "postgres://postgres"

from authenticator import authenticator
from main import app


class FakeCommentRepository:
    def __call__(self):
        return self

    def get_comments_by_account_id(self, account_id):
        comment_out = [
            {
                "id": "1",
                "comment": "hamburgers rule",
                "movie_api_id": "1",
                "account_id": "1",
            }
        ]
        return comment_out


def test_no_auth_get_comments_by_account_id():
    app.dependency_overrides[CommentRepository] = FakeCommentRepository()
    client = TestClient(app)
    response = client.get("/api/comments/mine")
    print(response.json())

    assert response.status_code == 401
    del client


def test_get_comments_by_account_id():
    app.dependency_overrides[CommentRepository] = FakeCommentRepository()
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = lambda: {"id": 1}
    client = TestClient(app)
    response = client.get("/api/comments/mine")
    print(response.json())

    assert response.status_code == 200
    del client


def test_get_specific_comments_by_account_id():
    app.dependency_overrides[CommentRepository] = FakeCommentRepository()
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = lambda: {"id": 1}

    client = TestClient(app)
    response = client.get("/api/comments/mine")
    data = response.json()

    expected_data = [
        {
            "id": "1",
            "comment": "hamburgers rule",
            "movie_api_id": "1",
            "account_id": 1,
        }
    ]

    assert data == expected_data
    del client
